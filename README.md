An online multiplayer game that put two remote players in a room to solve a puzzle and uses the Unreal Engine 4 multiplayer features, the Steamworks API and C++.

* Description

Secret Lab is a pet project created using C++ and the Unreal Engine 4 for the purpose of learning the basics of online multiplayer games.

In it two players are put together in a secret laboratory where one hosts a sessionand the other finds and joins the session and then need to work cooperatively to solve a puzzle. The project uses the Unreal Engine Online Subsystem and the Steamworks API as well as the core features of the engine and C++.

Since the main purpose of the project was to connect two players over the internet successfully, there is still only one simple puzzle (yet) to solve and there are no rewards whatsoever (yet) for solving it aside from the incredible feeling of solving a very easy puzzle.

* Installation

Since the game is still in an early stage to have it working you need to have the Unreal Engine 4.23 installed and use it to build the project. You can play directly in the engine or create an executable by, in the editor, going to File > Package Project and select the platform of your choosing. Please bear in mind that this is an early stage prototype so you may encounter some(or many) bugs.

- Important: Both players must have the Steam client open and logged in for the game to use the Steamworks API correctly.